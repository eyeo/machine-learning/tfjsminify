# Overview

Utility library to minify [TFJS](https://github.com/tensorflow/tfjs) and reduce it to its essential functionality for predefined target models.

# Usage

Place target models in the folder `models`

```
npm i
npm run minify
```

The minified version of TFJS can be found in the folder `dist`.

# Contents

 - `models/` hosts all models you want TFJS optimized for
 - `src/tfjs-partial.js` web bundler entry point importing TFJS's essential functionality
 - `src/tfjs-profile.js` generates a profile of TFJS based on the provided models
 - `src/tfjs-minify.js` minifies TFJS based on the generated profile
 - `src/http-util.js` utility file used during profiling

# Compatibility Information

Development environment
 - Node v16 or higher
 - NPM v7 or higher
 - Chrome >= v116 (headless chrome is used for in-browser profiling)

/*
 * This file is part of eyeo's tfjsMinify project,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * @eyeo/mlaf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @eyeo/mlaf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/mlaf.  If not, see <http://www.gnu.org/licenses/>.
 */

import commonjs from "@rollup/plugin-commonjs";
import {nodeResolve} from "@rollup/plugin-node-resolve";
import {terser} from "rollup-plugin-terser";
import {exec} from "child_process";
import * as util from "util";

const TFJS_PROFILE_GENERATE = "src/tfjs-profile.js";
const POST_PROFILE = "src/tfjs-minify.js";

export default {
  input: "src/tfjs-partial.js",
  output: {
    file: "dist/tfjs-partial.min.js",
    format: "es"
  },
  plugins: [
    commonjs(),
    nodeResolve(),
    terser({module: false, toplevel: false, compress: false}),
    {
      async writeBundle() {
        let syncExec = util.promisify(exec);
        let outputs = [
          await syncExec(`node ${TFJS_PROFILE_GENERATE}`),
          await syncExec(`node ${POST_PROFILE}`)
        ];
        for (let output of outputs) {
          process.stderr.write(output.stderr || "");
          process.stdout.write(output.stdout || "");
          if (output.stderr)
            process.exit();
        }
      }
    }
  ]
};
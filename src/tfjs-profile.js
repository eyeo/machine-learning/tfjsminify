/*
 * This file is part of eyeo's tfjsMinify project,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * @eyeo/mlaf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @eyeo/mlaf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/mlaf.  If not, see <http://www.gnu.org/licenses/>.
 */

import {existsSync, mkdirSync, readdirSync, readFileSync, writeFileSync} from "fs";
import {spawn} from "child_process";
import {
  startHttpServer, closeHttpServer, GMLUTIL_QUERY_PARAM, MODEL_LIST_QUERY_PARAM, RESULTS_QUERY_PARAM, TFJS_QUERY_PARAM,
  PORT, TFJS_PARTIAL_FILE, HOST, DEBUG_PORT
} from "./http-util.js";
import {dirname, join} from "path";
import {fileURLToPath} from "url";

const TEMP_FOLDER = join(dirname(fileURLToPath(import.meta.url)), "temp");
const PROFILE_FILE = join(TEMP_FOLDER, "tfjs-profile.js");
const CHROME_TIMEOUT = 10000;

let functionExceptions = [
  "Long", // Class invoked during instantiation through another object
  "isLong", // Function utilized by Long$1
  "fromInt", // Function utilized by Long$1
  "fromNumber", // Function utilized by Long$1
  "fromBits", // Function utilized by Long$1
  "fromString", // Function utilized by Long$1
  "fromValue", // Function utilized by Long$1
  "_mergeNamespaces", // Function invoked during instantiation through another object
  "getQueryParams", // Function called during instantiation through another object
  "setTensorTracker", // Function called during instantiation
  "isBrowser", // Function called during instantiation through another object
  "registerBackend", // Function referenced during instantiation
  "pack", // Kernel Function referenced during instantiation
  "prod", // Kernel Function referenced during instantiation
  "unpack", // Kernel Function referenced during instantiation
  "select", // Kernel Function referenced during instantiation
  "batchMatMul", // Kernel Function referenced during instantiation
  "gatherV2", // Kernel Function referenced during instantiation
  "_fusedMatMul", // Kernel Function referenced during instantiation
  "fusedMatMul", // Kernel Function referenced during instantiation
  "registerKernel", // Function referenced during instantiation
  "getFlatIndexFrom3D", // Function referenced during instantiation
  "pad", // Used through kernel function
  "matMul", // Used through kernel function
  "dot", // Used through kernel function
  "tensor1d", // Used through kernel function
  "clone", // Used through kernel function
  "relu", // Used through kernel function
  "sigmoid", // Used through kernel function
  "browserHTTPRequest", // Used through kernel function
  "decodeParam", // Keep as there's an unremovable reference
  "sum", // Keep as there's an unremovable reference
  "stack", // Function referenced during instantiation
  "warn" // Function called only if a kernel function can't be registered
];

// eslint-disable-next-line space-before-function-paren
let inject = async () => {
  let windowBuiltIns = new Set();
  let tfjsFunctionsAll = new Set();
  let tfjsFunctionsUsed = new Set();
  let tfjsFunctionsUnused = new Set();
  let modelBundles;
  let graphMLUtil;

  async function loadTfjs() {
    return new Promise(res => {
      // Load via HTML tag so results will be evaluated on global scope
      // which is required for us to be able to iterate through all funcs
      document.querySelector("#status").innerHTML += "Loading TFJS";
      let tfjsImport = document.createElement("script");
      tfjsImport.setAttribute("src", TFJS_QUERY_PARAM);
      document.body.appendChild(tfjsImport);
      tfjsImport.addEventListener("load", res);
    });
  }

  async function loadGmlu() {
    document.querySelector("#status").innerHTML += "<br />Loading GraphMLUtil";
    return import(GMLUTIL_QUERY_PARAM).then(ret => graphMLUtil = ret);
  }

  async function loadModels() {
    document.querySelector("#status").innerHTML += "<br />Loading Model List";
    return import(MODEL_LIST_QUERY_PARAM).then(ret => modelBundles = ret.bundles);
  }

  async function profileTfjs() {
    // Gather all additional global functions added since onload
    Object.keys(window).filter(e => !windowBuiltIns.has(e) && typeof window[e] === "function")
      .forEach(e => tfjsFunctionsAll.add(e));

    // Wrap TFJS's functions to record which ones were called during inference
    for (let fn of tfjsFunctionsAll) {
      let fnBackup = window[fn];
      window[fn] = function() {
        // Mark function as used
        tfjsFunctionsUsed.add(fn);
        return fnBackup.apply(this, arguments);
      };
    }
    // Wrap TFJS's exports as well
    for (let fn of Object.keys(tfjsPartial)) {
      tfjsFunctionsAll.add(fn);
      let fnBackup = tfjsPartial[fn];
      tfjsPartial[fn] = function() {
        // Mark function as used
        tfjsFunctionsUsed.add(fn);
        return fnBackup.apply(tfjsPartial, arguments);
      };
    }

    // Run inference
    for (let [modelName, modelBundle] of Object.entries(modelBundles)) {
      document.querySelector("#status").innerHTML += `<br /><br />Profiling ${modelName}`;
      tfjsPartial.enableProdMode();
      tfjsPartial.env().set("WEBGL_USE_SHAPES_UNIFORMS", true);
      document.querySelector("#status").innerHTML += "<br /> - Running inference via http";
      let parsedModel = await graphMLUtil.loadBundledModel(tfjsPartial, modelBundle);
      await graphMLUtil.inference(tfjsPartial, parsedModel, modelBundle, document.querySelector("body>div"), "*");
    }

    // Mark unused functions
    Object.keys(window)
      .filter(e => !windowBuiltIns.has(e) && !tfjsFunctionsUsed.has(e) && typeof window[e] === "function")
      .forEach(e => tfjsFunctionsUnused.add(e));

    // Add exceptions for allowlisted functions
    for (let exception of functionExceptions) {
      if (tfjsFunctionsUnused.has(exception))
        tfjsFunctionsUnused.delete(exception);
      if (!tfjsFunctionsUsed.has(exception))
        tfjsFunctionsUsed.add(exception);
    }

    // Move function references from unused to used if they exist in both
    for (let fn of tfjsFunctionsUsed) {
      if (tfjsFunctionsUnused.has(`${fn}_`)) {
        tfjsFunctionsUnused.delete(`${fn}_`);
        tfjsFunctionsUsed.add(`${fn}_`);
      }
      if (fn.slice(-1) === "_" && tfjsFunctionsUnused.has(`${fn.slice(0, -1)}`)) {
        tfjsFunctionsUnused.delete(`${fn.slice(0, -1)}`);
        tfjsFunctionsUsed.add(`${fn.slice(0, -1)}`);
      }
      if (tfjsFunctionsUnused.has(`${fn}Impl`)) {
        tfjsFunctionsUnused.delete(`${fn}Impl`);
        tfjsFunctionsUsed.add(`${fn}Impl`);
      }
      for (let dupe of [...tfjsFunctionsUnused].filter(e => e.slice(-2, -1) === "$" && e.slice(0, -2) === fn)) {
        tfjsFunctionsUnused.delete(dupe);
        tfjsFunctionsUsed.add(dupe);
      }
    }

    document.querySelector("#status").innerHTML += `<br /><br />Finished Profiling:<br /> - Unused Functions: ${tfjsFunctionsUnused.size}<br /> - Used Functions: ${tfjsFunctionsUsed.size}`;
    document.querySelector("#status").innerHTML +=
      "<br /><br />This window will be closed in 10 seconds.";

    fetch(RESULTS_QUERY_PARAM, {
      method: "POST",
      headers: {"Content-Type": "application/json"},
      body: JSON.stringify({
        functionsUsed: [...tfjsFunctionsUsed],
        functionsUnused: [...tfjsFunctionsUnused]
      }, null, 2)
    });

    setTimeout(() => {
      window.close();
    }, 10000);
  }

  window.addEventListener("load", e => {
    // Record all window built-ins to compare them with all functions
    // added by tfjs on the global scope
    Object.keys(window).forEach(a => windowBuiltIns.add(a));

    // Asynchroneously load TFJS, GMLU, models and start profiling
    loadTfjs()
      .then(loadGmlu.bind(this))
      .then(loadModels.bind(this))
      .then(profileTfjs.bind(this));
  });
};

function prepareScriptToInject() {
  let scriptToInject = inject.toString().split("\n").slice(1, -1).join("\n");
  scriptToInject = scriptToInject.replaceAll("TFJS_QUERY_PARAM", `"${TFJS_QUERY_PARAM}"`);
  scriptToInject = scriptToInject.replaceAll("GMLUTIL_QUERY_PARAM", `"${GMLUTIL_QUERY_PARAM}"`);
  scriptToInject = scriptToInject.replaceAll("MODEL_LIST_QUERY_PARAM", `"${MODEL_LIST_QUERY_PARAM}"`);
  scriptToInject = scriptToInject.replaceAll("RESULTS_QUERY_PARAM", `"${RESULTS_QUERY_PARAM}"`);
  // Parse TFJS and add exceptions for functions called
  // through CONSTs as they aren't caught in our browser test.
  let tfjs = readFileSync(TFJS_PARTIAL_FILE).toString();
  let regex = /const [a-zA-Z0-9_-]{1,}=([a-zA-Z0-9_-]{1,})\(/g;
  let match;
  while ((match = regex.exec(tfjs)) !== null) {
    if (!functionExceptions.includes(match[1]))
      functionExceptions.push(match[1]);
  }
  // Add exceptions for functions which are called during instantiation
  regex = /[;|}]([a-zA-Z0-9_-]{1,})\([a-zA-Z0-9.,_-]{0,}\);/g;
  while ((match = regex.exec(tfjs)) !== null) {
    if (!functionExceptions.includes(match[1]))
      functionExceptions.push(match[1]);
  }
  scriptToInject = scriptToInject.replaceAll("functionExceptions", `${JSON.stringify(functionExceptions)}`);
  return scriptToInject;
}

let chromeInstance;

function close() {
  if (chromeInstance)
    chromeInstance.kill("SIGINT");
  closeHttpServer();
  process.exit();
}

startHttpServer(prepareScriptToInject(), CHROME_TIMEOUT).then(
  data => {
    let jsonData = JSON.parse(data);
    let models = readdirSync(join(dirname(fileURLToPath(import.meta.url)), "..", "models"))
      .filter(e => e.endsWith(".dat")).join(", ");
        process.stdout.write("\x1b[35mTFJS Profiling finished\n");
        process.stdout.write(`\x1b[35mModel/s tested: ${models}\n`);
        process.stdout.write(`\x1b[35mFunctions in use: ${jsonData.functionsUsed.length}\n`);
        process.stdout.write(`\x1b[35mFunctions removed: ${jsonData.functionsUnused.length}\n`);

    if (!existsSync(TEMP_FOLDER))
      mkdirSync(TEMP_FOLDER);
    writeFileSync(PROFILE_FILE, `const PROFILE = ${data};\n\nexport default PROFILE;\n`);

    process.exit();
  }
).catch(err => {
  process.stderr.write(`\x1b[31mError: ${err}.\n`);
  close();
});

let cmdChrome;
if (process.platform === "win32")
  cmdChrome = ["start", "/MIN", "chrome", `"http://${HOST}:${PORT}"`];
else if (process.platform === "darwin")
  cmdChrome = ["/Applications/Google Chrome.app/Contents/MacOS/Google Chrome", `http://${HOST}:${PORT}`, "--headless", `--remote-debugging-port=${DEBUG_PORT}`, "--use-angle=default"];
else
  cmdChrome = ["google-chrome", `http://${HOST}:${PORT}`];

try {
  chromeInstance = spawn(cmdChrome[0], cmdChrome.slice(1), {shell: process.platform === "win32"});
}
catch (e) {
  // Invoking spawn fails
  process.stderr.write(`\x1b[31mError: Subprocess can't be run (${e}).\n`);
  close();
}
chromeInstance.stderr.on("data", data => {
  // Chrome can be invoked but returns an error
  if (!data.includes("listening on ws") && !data.includes("WebSwapCGLLayer") && !data.includes("Floss manager")) {
    // Ignoring specific logging/warning output that headless chrome incorrectly prints to stderr
    process.stderr.write(`\x1b[31mError: Chrome can't be run (${data}).\n`);
    close();
  }
});
chromeInstance.on("error", data => {
  // Chrome can't be found or can't be invoked
  process.stderr.write(`\x1b[31mError: Chrome can't be found (${data}).\n`);
  close();
});

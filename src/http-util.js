/*
 * This file is part of eyeo's tfjsMinify project,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * @eyeo/mlaf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @eyeo/mlaf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/mlaf.  If not, see <http://www.gnu.org/licenses/>.
 */

import http from "http";
import {readFileSync, readdirSync} from "fs";
import {dirname, join} from "path";
import {fileURLToPath} from "url";

export const HOST = "localhost";
export const DEBUG_PORT = 9222;
export const PORT = 8988;

const GRAPHMLUTIL_FILE =
  join(dirname(fileURLToPath(import.meta.url)), "..", "node_modules", "@eyeo", "mlaf", "lib", "library", "graphml-util.min.js");
export const TFJS_PARTIAL_FILE = join(dirname(fileURLToPath(import.meta.url)), "..", "dist", "tfjs-partial.min.js");
export const ROOT_QUERY_PARAM = "/";
export const MODEL_LIST_QUERY_PARAM = "/models";
export const TFJS_QUERY_PARAM = "/tfjs";
export const GMLUTIL_QUERY_PARAM = "/gmlutil";
export const RESULTS_QUERY_PARAM = "/results";

let serverInstance;

export function closeHttpServer() {
  serverInstance.close();
}

export function startHttpServer(scriptToInject, timeoutMillis) {
  // We need to profile TFJS in a browser to get accurate results,
  // so we spawn an HTTP server which provides:
  // - A simple website, hosting a profiling script
  // - Request handlers for needed assets (TFJS, GMLUtil, model files)
  // - A callback to receive and store profiling results
  return new Promise((resolve, reject) => {
    let timeoutId = setTimeout(() => {
      clearTimeout(timeoutId);
      reject("Chrome unresponsive, waited for " + timeoutMillis + "ms.");
    }, timeoutMillis);
    serverInstance = http.createServer((req, res) => {
      let path = req.url;
      let type = "text/html";
      let ret = null;
      if (path === ROOT_QUERY_PARAM) {
        // Request for index.html
        // Returning a simple page with `inject` pre-injected.
        ret = `<html lang="en"><head><script type="text/javascript">${scriptToInject}</script><title></title></head><body style="height: 100%; font-family: Arial, Helvetica, sans-serif; display: flex; justify-content: center; align-items: center;"><div id="hook" style="display: none;"></div><div style="width: 600px; background-color: #eeeeee; border-radius: 20px; padding: 20px;"><div style="font-size: 25px;">TFJS Profiler</div><div id="status" style="font-size: 18px;"><br /></div></div></body></html>`;
      }
      else if (path === TFJS_QUERY_PARAM) {
        // Request for tfjsPartial.js
        type = "text/javascript";
        ret = readFileSync(TFJS_PARTIAL_FILE).toString();
        // Turn exports into an object which can be consumed by CJS
        let exports = (/export\{(.*)\};/g).exec(ret)[1].split(",").map(e => e.split(" as ")
          .length === 1 ? [e, e] : e.split(" as ")).map(e => `${e[1]}: ${e[0]}`).join(",");
        ret = ret.replace(/export\{.*/g, "");
        ret += `\nlet tfjsPartial = {${exports || "{}"}};`;
      }
      else if (path === GMLUTIL_QUERY_PARAM) {
        // Request for graphmlutil.js
        type = "text/javascript";
        ret = readFileSync(GRAPHMLUTIL_FILE).toString();
      }
      else if (path === MODEL_LIST_QUERY_PARAM) {
        // Request for list of all model json files + content
        type = "text/javascript";
        ret = {};
        let files = readdirSync(join(dirname(fileURLToPath(import.meta.url)), "..", "models"));
        for (let file of files) {
          ret[file] = JSON.parse(
            readFileSync(join(dirname(fileURLToPath(import.meta.url)), "..", "models", file)).toString());
        }
        ret = `export let bundles = ${JSON.stringify(ret)}`;
      }
      else if (path.endsWith(".json")) {
        // Request for model json files
        type = "application/json";
        ret = readFileSync(join(dirname(fileURLToPath(import.meta.url)), "..", "models", path.slice(1))).toString();
      }
      else if (path.endsWith(".js")) {
        // Request for model config files
        type = "text/javascript";
        ret = readFileSync(join(dirname(fileURLToPath(import.meta.url)), "..", "models", path.slice(1))).toString();
      }
      else if (path.endsWith(".dat")) {
        // Request for model dat files
        type = "application/octet-stream";
        ret = readFileSync(join(dirname(fileURLToPath(import.meta.url)), "..", "models", path.slice(1)));
      }
      else if (path === RESULTS_QUERY_PARAM) {
        // Request containing profiling data
        // Write results to ./temp/tfjs-profile.js and exit
        let data = "";
        req.on("data", chunk => {
          data += chunk;
        });
        req.on("end", () => {
          res.writeHead(200, {"Content-Type": "text/plain"});
          res.write("");
          res.end();
          closeHttpServer();
          clearTimeout(timeoutId);
          resolve(data);
        });
        return;
      }
      else if (path === "/favicon.ico") {
        // Dummy favicon request handler so Chrome stops printing errors
        type = "text/plain";
        ret = "";
      }

      // Return request
      res.writeHead(ret !== null ? 200 : 500, {"Content-Type": `${type}`});
      res.write(ret || "", type === "application/octet-stream" ? "binary" : null);
      res.end();
    });
    serverInstance.listen(PORT);
  });
}

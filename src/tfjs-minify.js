/*
 * This file is part of eyeo's tfjsMinify project,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * @eyeo/mlaf is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * @eyeo/mlaf is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with @eyeo/mlaf.  If not, see <http://www.gnu.org/licenses/>.
 */

import {readFileSync, writeFile} from "fs";
import {join, dirname} from "path";
import {fileURLToPath} from "url";
import profile from "./temp/tfjs-profile.js";

const PACKAGE_FILE = JSON.parse(readFileSync("./package.json"));
const TFJS_FILE = join(dirname(fileURLToPath(import.meta.url)), "..", "dist", "tfjs-partial.min.js");
const FUNCTION_CUTOFF = 200;
const LICENSE = `   /**  
    *   ${PACKAGE_FILE.name} - v${PACKAGE_FILE.version}
    *   ${PACKAGE_FILE.description}
    *   Made by ${PACKAGE_FILE.author}
    *   Under ${PACKAGE_FILE.license} License
    *   
    *   The following npm packages may be included in this product:
    *
    *   - tfjs
    *   - tfjs-backend-cpu
    *   - tfjs-backend-webgl
    *   - tfjs-converter
    *   - tfjs-core
    *   - tfjs-data
    *   - tfjs-layers
    *
    *    These packages contain the following information about the license:
    * 
    *  * @license
    *  * Copyright 2018-2022 Google LLC. All Rights Reserved.
    *  * Licensed under the Apache License, Version 2.0 (the "License");
    *  * you may not use this file except in compliance with the License.
    *  * You may obtain a copy of the License at
    *  *
    *  * http://www.apache.org/licenses/LICENSE-2.0
    *  *
    *  * Unless required by applicable law or agreed to in writing, software
    *  * distributed under the License is distributed on an "AS IS" BASIS,
    *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    *  * See the License for the specific language governing permissions and
    *  * limitations under the License.
    *  * =============================================================================
    */ 
`;

let amendedTfjsPartial = LICENSE + readFileSync(TFJS_FILE).toString();
// Remove google license comments
amendedTfjsPartial = amendedTfjsPartial.replace(/\s*\/\*\*\n\s+\*\s@license\n(\s+\*?.+\n)+\s+\*\//gm, "\n");
// Remove unused functions
for (let func of profile.functionsUnused) {
  // Sometimes functions have a postfix, which we need to skip
  if (profile.functionsUsed.includes(func.replace(/_$/, "")))
    continue;
  let terms = [
    `async function ${func}(`,
    `function ${func}(`
  ];
  for (let term of terms) {
    while (amendedTfjsPartial.indexOf(term) !== -1) {
      let count = 1;
      let start = amendedTfjsPartial.indexOf(term);
      let index = amendedTfjsPartial.indexOf(")", start);
      index = amendedTfjsPartial.indexOf("{", index);
      // Abort if function doesn't have curly braces.
      // Cutoff at 200 works fine with this code base.
      // Might require a higher threshold once it turns
      // greedy. Alternatively use a fully-fledged JS
      // parser instead.
      if (index === -1 || index - start > FUNCTION_CUTOFF)
        break;
      while (count !== 0 && index < amendedTfjsPartial.length) {
        index += 1;
        if (amendedTfjsPartial.charAt(index) === "{")
          count += 1;
        else if (amendedTfjsPartial.charAt(index) === "}")
          count -= 1;
      }
      amendedTfjsPartial = amendedTfjsPartial.replace(amendedTfjsPartial.substring(start, index + 1), "");
    }
  }
  // Remove const declarations
  let escapedFunc = func.replace(/_$/, "");
  escapedFunc = escapedFunc.replace(/\$[0-9]$/, "");
  amendedTfjsPartial = amendedTfjsPartial.replaceAll(new RegExp(`^const ${escapedFunc}[_]?[$]?[0-9]?=${escapedFunc}[_]?[$]?[0-9]?;`, "gm"), "");
}
// Remove const remnants
amendedTfjsPartial = amendedTfjsPartial.replace(/^const [^;]*\}\);/gm, "");
for (let func of profile.functionsUnused) {
  amendedTfjsPartial = amendedTfjsPartial.replace(`const ${func}=${func}_;`, "");
  amendedTfjsPartial = amendedTfjsPartial.replace(`const ${func.replace("_", "")}=${func};`, "");
}
// Remove unused exports
let exports = (/export\{(.*)\};/g).exec(amendedTfjsPartial)[1].split(",").map(e => e.split(" as ")
  .length === 1 ? [e, e] : e.split(" as ")).map(e => e[1]);
for (let func of exports) {
  if (profile.functionsUnused.includes(func) || (profile.functionsUnused.includes(`${func}_`) && !profile.functionsUsed.includes(func))) {
    let str = (/export\{(.*)\};/g).exec(amendedTfjsPartial)[1];
    amendedTfjsPartial = amendedTfjsPartial.replace(str, str.split(",").filter(e => !e.includes(func)).join(","));
  }
}
// Change dict references of unused functions to noop
for (let func of profile.functionsUnused) {
  let escapedFunc = func.replace(/_$/, "");
  escapedFunc = escapedFunc.replace(/\$[0-9]$/, "");
  amendedTfjsPartial = amendedTfjsPartial.replaceAll(new RegExp(`${escapedFunc}[_]?[$]?[0-9]?:${escapedFunc}[_]?[$]?[0-9]?,`, "gm"), `${func}:()=>{},`);
  amendedTfjsPartial = amendedTfjsPartial.replaceAll(new RegExp(`${escapedFunc}[_]?[$]?[0-9]?:${escapedFunc}[_]?[$]?[0-9]?}`, "gm"), `${func}:()=>{}}`);
}
// Remove emopty lines
amendedTfjsPartial = amendedTfjsPartial.replace(/^$\n/gm, "");
// Forcefully remove node fetch
amendedTfjsPartial = amendedTfjsPartial.replace(/^const getNodeFetch.*?\n/gm, "");

writeFile(
  TFJS_FILE,
  amendedTfjsPartial,
  error => {
    if (error)
      process.exit(1);
  }
);
